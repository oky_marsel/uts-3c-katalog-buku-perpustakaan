package marsel.oky.puskots

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.crud.*
import kotlinx.android.synthetic.main.katalog.*

class KatalogActivity : AppCompatActivity(), View.OnClickListener {


    val COLLECTION = "uts"
    val F_ID = "id"
    val F_JUDUL = "judul"
    val F_PENULIS = "penulis"
    val F_TAHUN = "tahun"
    val F_PENERBIT = "penerbit"
    val F_NAME1 = "file_name1"
    val F_TYPE1 = "file_type1"
    val F_URL1 = "file_url1"
    val F_NAME2 = "file_name2"
    val F_TYPE2 = "file_type2"
    val F_URL2 = "file_url2"
    val RC_OK1 = 101
    val RC_OK2 = 102
    var fileType1 =""
    var fileName1 =""
    var fileType2 =""
    var fileName2 =""
    var docId = ""
    var editImg : Boolean = false
    var editPdf : Boolean = false
    lateinit var db : FirebaseFirestore
    lateinit var storage : StorageReference
    lateinit var uri1 : Uri
    lateinit var uri2 : Uri
    lateinit var alCoba :  ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapterr

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.katalog)
//        ls_katalog.setOnItemClickListener(itemClick)
        alCoba = ArrayList()
        uri1 = Uri.EMPTY
        uri2 = Uri.EMPTY
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore", e.message.toString())
            ShowData()
        }
    }

//    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
//        val hm = alCoba.get(position)
//        docId = hm.get(F_ID).toString()
//        edIdBuku.setText(docId)
//        edJudulBuku.setText(hm.get(F_JUDUL).toString())
//        edPenulisBuku.setText(hm.get(F_PENULIS).toString())
//        edPenerbitBuku.setText(hm.get(F_PENERBIT).toString())
//        edTahunBuku.setText(hm.get(F_TAHUN).toString())
//        fileType1 = hm.get(F_TYPE1).toString()
//        fileName1 = hm.get(F_NAME1).toString()
//        fileType2 = hm.get(F_TYPE2).toString()
//        fileName2 = hm.get(F_NAME2).toString()
//        edNameImg.setText(hm.get(F_NAME1).toString()+hm.get(F_TYPE1).toString())
//        edNamePdf.setText(hm.get(F_NAME2).toString()+hm.get(F_TYPE2).toString())
//    }

    fun ShowData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alCoba.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_JUDUL,doc.get(F_JUDUL).toString())
                hm.set(F_PENULIS,doc.get(F_PENULIS).toString())
                hm.set(F_PENERBIT,doc.get(F_PENERBIT).toString())
                hm.set(F_TAHUN,doc.get(F_TAHUN).toString())
                hm.put(F_NAME1, doc.get(F_NAME1).toString())
                hm.put(F_TYPE1, doc.get(F_TYPE1).toString())
                hm.put(F_URL1, doc.get(F_URL1).toString())
                hm.put(F_NAME2, doc.get(F_NAME2).toString())
                hm.put(F_TYPE2, doc.get(F_TYPE2).toString())
                hm.put(F_URL2, doc.get(F_URL2).toString())
                alCoba.add(hm)
            }
            adapter = CustomAdapterr(this,alCoba)
            ls_katalog.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK1)){
//            if(data != null) {
//                uri1 = data.data!!
//                edNameImg.setText(uri1.toString())
//                editImg = true
//            }
//        }
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK2)){
            if(data != null) {
                uri2 = data.data!!
//                edNamePdf.setText(uri2.toString())
//                editPdf = true
            }
        }
    }



}