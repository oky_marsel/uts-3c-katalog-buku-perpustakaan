package marsel.oky.puskots

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btKatalog.setOnClickListener(this)
        btLogIn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btLogIn -> {
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btKatalog -> {
                val intent = Intent(this,KatalogActivity::class.java)
                startActivity(intent)
            }
        }

    }
}