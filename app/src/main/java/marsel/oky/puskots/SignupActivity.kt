package marsel.oky.puskots

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.admin.*
import kotlinx.android.synthetic.main.crud.*
import kotlinx.android.synthetic.main.signup.*

class SignupActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btRegister -> {
                var email = regUsername.text.toString()
                var password = regPassword.text.toString()

                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username / password can't be empty", Toast.LENGTH_LONG).show()
                }
                else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Registering...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(){
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(this, "Succesfully Register", Toast.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener{
                            progressDialog.hide()
                            Toast.makeText(this,"Username / Password incorrect", Toast.LENGTH_SHORT).show()
                        }
                }
                regUsername.setText("")
                regPassword.setText("")
            }
            R.id.btBackAdmin -> {
                val intent = Intent(this,AdminActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup)
        btRegister.setOnClickListener(this)
        btBackAdmin.setOnClickListener(this)
    }


}