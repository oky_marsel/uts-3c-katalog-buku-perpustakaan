package marsel.oky.puskots

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btSignIn -> {
                var email : String = txUsername.text.toString()
                var password : String = txPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username/Password can't be empty", Toast.LENGTH_LONG).show()
                } else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            progressDialog.hide()
                            if(!it.isSuccessful) return@addOnCompleteListener
                            txUsername.setText("")
                            txPassword.setText("")
                            txUsername.requestFocus()
                            Toast.makeText(this,"Successfully Login", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this,AdminActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this, "Username/Password incorrect", Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.btBackHome -> {
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        btSignIn.setOnClickListener(this)
        btBackHome.setOnClickListener(this)
    }




}