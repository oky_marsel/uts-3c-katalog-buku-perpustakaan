package marsel.oky.puskots

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapter(val context : Context, arrayList: ArrayList<HashMap<String,Any>>) : BaseAdapter() {

    val F_ID = "id"
    val F_JUDUL = "judul"
    val F_PENULIS = "penulis"
    val F_TAHUN = "tahun"
    val F_PENERBIT = "penerbit"
    val F_NAME1 = "file_name1"
    val F_TYPE1 = "file_type1"
    val F_URL1 = "file_url1"
    val F_NAME2 = "file_name2"
    val F_TYPE2 = "file_type2"
    val F_URL2 = "file_url2"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txId : TextView? = null
        var txJudul : TextView? = null
        var txPenulis : TextView? = null
        var txTahun : TextView? = null
        var txPenerbit : TextView? = null
        var txNamaPdf : TextView? = null
        var txImg : ImageView? = null
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if(convertView == null){
            var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view  = inflater.inflate(R.layout.row_crud,null,true)
            holder.txId = view!!.findViewById(R.id.txId) as TextView
            holder.txJudul = view!!.findViewById(R.id.txJudul) as TextView
            holder.txPenulis = view!!.findViewById(R.id.txPenulis) as TextView
            holder.txTahun = view!!.findViewById(R.id.txTahun) as TextView
            holder.txPenerbit = view!!.findViewById(R.id.txPenerbit) as TextView
            holder.txNamaPdf = view!!.findViewById(R.id.txNamaPdf) as TextView
            holder.txImg = view!!.findViewById(R.id.txImg) as ImageView
            view.tag = holder
        }
        else {
            holder   = view!!.tag as ViewHolder
        }

        var fileType = list.get(position).get(F_TYPE1).toString()
        uri = Uri.parse(list.get(position).get(F_URL1).toString())
        holder.txId!!.setText(list.get(position).get(F_ID).toString())
        holder.txJudul!!.setText(list.get(position).get(F_JUDUL).toString())
        holder.txPenulis!!.setText(list.get(position).get(F_PENULIS).toString())
        holder.txTahun!!.setText(list.get(position).get(F_TAHUN).toString())
        holder.txPenerbit!!.setText(list.get(position).get(F_PENERBIT).toString())
        holder.txNamaPdf!!.setText(list.get(position).get(F_NAME2).toString()+list.get(position).get(F_TYPE2).toString())

        when(fileType){
//            ".pdf" -> {holder.imv!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
//            ".docx" -> {holder.imv!!.setImageResource(android.R.drawable.ic_menu_edit)}
//            ".mp4" -> {holder.imv!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {Picasso.get().load(uri).into(holder.txImg)}
        }
        return view!!
    }
}