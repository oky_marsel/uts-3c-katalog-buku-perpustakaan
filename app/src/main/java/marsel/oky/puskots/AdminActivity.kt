package marsel.oky.puskots

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.admin.*

class AdminActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btKelola -> {
                val intent = Intent(this,CrudActivity::class.java)
                startActivity(intent)
            }
            R.id.btSignUp -> {
                val intent = Intent(this,SignupActivity::class.java)
                startActivity(intent)
            }
            R.id.btLogOut -> {
                fbAuth.signOut()
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin)
        btKelola.setOnClickListener(this)
        btSignUp.setOnClickListener(this)
        btLogOut.setOnClickListener(this)
    }



}