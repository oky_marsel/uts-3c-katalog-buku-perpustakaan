package marsel.oky.puskots

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class CustomAdapterr(val context : Context, arrayList: ArrayList<HashMap<String,Any>>) : BaseAdapter() {

    val F_ID = "id"
    val F_JUDUL = "judul"
    val F_PENULIS = "penulis"
    val F_TAHUN = "tahun"
    val F_PENERBIT = "penerbit"
    val F_NAME1 = "file_name1"
    val F_TYPE1 = "file_type1"
    val F_URL1 = "file_url1"
    val F_NAME2 = "file_name2"
    val F_TYPE2 = "file_type2"
    val F_URL2 = "file_url2"
    val list = arrayList
    var uri = Uri.EMPTY
    var uri2 = Uri.EMPTY


    inner class ViewHolder(){
        var txKatalogId : TextView? = null
        var txKatalogJudul : TextView? = null
        var txKatalogPenulis : TextView? = null
        var txKatalogTahun : TextView? = null
        var txKatalogPenerbit : TextView? = null
        var txKatalogNamaPdf : TextView? = null
        var txFileUrl : TextView? = null
        var txKatalogImg : ImageView? = null
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if(convertView == null){
            var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view  = inflater.inflate(R.layout.row_katalog,null,true)
            holder.txKatalogId = view!!.findViewById(R.id.txkatalogId) as TextView
            holder.txKatalogJudul = view!!.findViewById(R.id.txKatalogJudul) as TextView
            holder.txKatalogPenulis = view!!.findViewById(R.id.txKatalogPenulis) as TextView
            holder.txKatalogTahun = view!!.findViewById(R.id.txKatalogTahun) as TextView
            holder.txKatalogPenerbit = view!!.findViewById(R.id.txKatalogPenerbit) as TextView
            holder.txKatalogNamaPdf = view!!.findViewById(R.id.txKatalogNamaPdf) as TextView
            holder.txFileUrl = view!!.findViewById(R.id.txKatalogUrl) as TextView
            holder.txKatalogImg = view!!.findViewById(R.id.txKatalogImg) as ImageView
            view.tag = holder
        }
        else {
            holder   = view!!.tag as ViewHolder
        }

        var fileType = list.get(position).get(F_TYPE1).toString()
        uri = Uri.parse(list.get(position).get(F_URL1).toString())
//        var fileType2 = list.get(position).get(F_TYPE2).toString()
        uri2 = Uri.parse(list.get(position).get(F_URL2).toString())
        holder.txKatalogId!!.setText(list.get(position).get(F_ID).toString())
        holder.txKatalogJudul!!.setText(list.get(position).get(F_JUDUL).toString())
        holder.txKatalogPenulis!!.setText(list.get(position).get(F_PENULIS).toString())
        holder.txKatalogTahun!!.setText(list.get(position).get(F_TAHUN).toString())
        holder.txKatalogPenerbit!!.setText(list.get(position).get(F_PENERBIT).toString())
        holder.txKatalogNamaPdf!!.setText(list.get(position).get(F_NAME2).toString()+list.get(position).get(F_TYPE2).toString())
        holder.txFileUrl!!.setText(uri2.toString())
        holder.txFileUrl!!.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_VIEW).setData(Uri.parse(holder.txFileUrl!!.text.toString()))
            context.startActivity(intent)
        }
        when(fileType){
//            ".pdf" -> {holder.imv!!.setImageResource(android.R.drawable.ic_dialog_dialer)}
//            ".docx" -> {holder.imv!!.setImageResource(android.R.drawable.ic_menu_edit)}
//            ".mp4" -> {holder.imv!!.setImageResource(android.R.drawable.ic_media_play)}
            ".jpg" -> {Picasso.get().load(uri).into(holder.txKatalogImg)}
        }
        return view!!
    }
}