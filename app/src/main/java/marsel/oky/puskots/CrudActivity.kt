package marsel.oky.puskots

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.crud.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CrudActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "uts"
    val F_ID = "id"
    val F_JUDUL = "judul"
    val F_PENULIS = "penulis"
    val F_TAHUN = "tahun"
    val F_PENERBIT = "penerbit"
    val F_NAME1 = "file_name1"
    val F_TYPE1 = "file_type1"
    val F_URL1 = "file_url1"
    val F_NAME2 = "file_name2"
    val F_TYPE2 = "file_type2"
    val F_URL2 = "file_url2"
    val RC_OK1 = 101
    val RC_OK2 = 102
    var fileType1 =""
    var fileName1 =""
    var fileType2 =""
    var fileName2 =""
    var docId = ""
    var editImg : Boolean = false
    var editPdf : Boolean = false
    lateinit var db : FirebaseFirestore
    lateinit var storage : StorageReference
    lateinit var uri1 : Uri
    lateinit var uri2 : Uri
    lateinit var alCoba :  ArrayList<HashMap<String,Any>>
    lateinit var adapter: CustomAdapter


    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when (v?.id) {
            R.id.btUpImg -> {
                fileType1 = ".jpg"
                intent.setType("image/*")
                startActivityForResult(intent, RC_OK1)
            }
            R.id.btUpPdf -> {
                fileType2 = ".pdf"
                intent.setType("application/pdf")
                startActivityForResult(intent, RC_OK2)
            }
            R.id.btTambah -> {
                if (uri1 !=null && uri2 !=null){
                    fileName1 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef1 = storage.child(fileName1+fileType1)
                    fileRef1.putFile(uri1)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task1 ->
                            return@Continuation fileRef1.downloadUrl
                        })
                        .addOnCompleteListener { img ->
                            fileName2 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                            val fileRef2 = storage.child(fileName2+fileType2)
                            fileRef2.putFile(uri2)
                                .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task2 ->
                                    return@Continuation fileRef2.downloadUrl
                                })
                                .addOnCompleteListener { taks ->
                                    val  hm = HashMap<String,Any>()
                                    hm.set(F_ID, edIdBuku.text.toString())
                                    hm.set(F_JUDUL, edJudulBuku.text.toString())
                                    hm.set(F_PENULIS, edPenulisBuku.text.toString())
                                    hm.set(F_PENERBIT, edPenerbitBuku.text.toString())
                                    hm.set(F_TAHUN, edTahunBuku.text.toString())
                                    hm.put(F_NAME1, fileName1)
                                    hm.put(F_TYPE1,fileType1)
                                    hm.put(F_URL1, img.result.toString())
                                    hm.put(F_NAME2, fileName2)
                                    hm.put(F_TYPE2,fileType2)
                                    hm.put(F_URL2, taks.result.toString())
                                    db.collection(COLLECTION).document(edIdBuku.text.toString()).set(hm)
                                        .addOnSuccessListener {
                                            Toast.makeText(
                                                this,
                                                "Data Berhasil Added",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            edIdBuku.setText("")
                                            edJudulBuku.setText("")
                                            edPenulisBuku.setText("")
                                            edTahunBuku.setText("")
                                            edPenerbitBuku.setText("")
                                            edNameImg.setText("")
                                            edNamePdf.setText("")
                                        }.addOnFailureListener { e ->
                                            Toast.makeText(
                                                this,
                                                "Data Tidak Berhasil Added : ${e.message}",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            edIdBuku.setText("")
                                            edJudulBuku.setText("")
                                            edPenulisBuku.setText("")
                                            edTahunBuku.setText("")
                                            edPenerbitBuku.setText("")
                                            edNameImg.setText("")
                                            edNamePdf.setText("")
                                        }
                                }
                        }
                }
            }
            R.id.btUbah -> {
                    if(editImg  && editPdf){
                        FirebaseStorage.getInstance().reference.child(fileName1+fileType1).delete()
                        FirebaseStorage.getInstance().reference.child(fileName2+fileType2).delete()
                        fileName1 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                        val fileRef1 = storage.child(fileName1+fileType1)
                        fileRef1.putFile(uri1)
                            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task1 ->
                                return@Continuation fileRef1.downloadUrl
                            })
                            .addOnCompleteListener { img ->
                                fileName2 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                                val fileRef2 = storage.child(fileName2+fileType2)
                                fileRef2.putFile(uri2)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task2 ->
                                        return@Continuation fileRef2.downloadUrl
                                    })
                                    .addOnCompleteListener { taks ->
                                        val hm = HashMap<String, Any>()
                                        hm.set(F_ID, edIdBuku.text.toString())
                                        hm.set(F_JUDUL, edJudulBuku.text.toString())
                                        hm.set(F_PENULIS, edPenulisBuku.text.toString())
                                        hm.set(F_PENERBIT, edPenerbitBuku.text.toString())
                                        hm.set(F_TAHUN, edTahunBuku.text.toString())
                                        hm.put(F_NAME1, fileName1)
                                        hm.put(F_TYPE1,fileType1)
                                        hm.put(F_URL1, img.result.toString())
                                        hm.put(F_NAME2, fileName2)
                                        hm.put(F_TYPE2,fileType2)
                                        hm.put(F_URL2, taks.result.toString())
                                        db.collection(COLLECTION).document(docId).update(hm)
                                            .addOnSuccessListener {
                                                Toast.makeText(this, "Data Berhasil Updated", Toast.LENGTH_SHORT)
                                                    .show()
                                                edIdBuku.setText("")
                                                edJudulBuku.setText("")
                                                edPenulisBuku.setText("")
                                                edTahunBuku.setText("")
                                                edPenerbitBuku.setText("")
                                                edNameImg.setText("")
                                                edNamePdf.setText("")
                                            }.addOnFailureListener { e ->
                                                Toast.makeText(
                                                    this,
                                                    "Data Tidak Berhasil Updated : ${e.message}",
                                                    Toast.LENGTH_SHORT
                                                )
                                                    .show()
                                                edIdBuku.setText("")
                                                edJudulBuku.setText("")
                                                edPenulisBuku.setText("")
                                                edTahunBuku.setText("")
                                                edPenerbitBuku.setText("")
                                                edNameImg.setText("")
                                                edNamePdf.setText("")
                                            }
                                    }
                            }
                    }

                else if(editImg){
                        FirebaseStorage.getInstance().reference.child(fileName1+fileType1).delete()
//                        FirebaseStorage.getInstance().reference.child(fileName2+fileType2).delete()
                    fileName1 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef1 = storage.child(fileName1+fileType1)
                    fileRef1.putFile(uri1)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task1 ->
                            return@Continuation fileRef1.downloadUrl
                        })
                        .addOnCompleteListener { img ->
                            val hm = HashMap<String, Any>()
                            hm.set(F_ID, edIdBuku.text.toString())
                            hm.set(F_JUDUL, edJudulBuku.text.toString())
                            hm.set(F_PENULIS, edPenulisBuku.text.toString())
                            hm.set(F_PENERBIT, edPenerbitBuku.text.toString())
                            hm.set(F_TAHUN, edTahunBuku.text.toString())
                            hm.put(F_NAME1, fileName1)
                            hm.put(F_TYPE1, fileType1)
                            hm.put(F_URL1, img.result.toString())
                            db.collection(COLLECTION).document(docId).update(hm)
                                .addOnSuccessListener {
                                    Toast.makeText(
                                        this,
                                        "Data Berhasil Updated",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    edIdBuku.setText("")
                                    edJudulBuku.setText("")
                                    edPenulisBuku.setText("")
                                    edTahunBuku.setText("")
                                    edPenerbitBuku.setText("")
                                    edNameImg.setText("")
                                    edNamePdf.setText("")
                                }.addOnFailureListener { e ->
                                    Toast.makeText(
                                        this,
                                        "Data Tidak Berhasil Updated : ${e.message}",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    edIdBuku.setText("")
                                    edJudulBuku.setText("")
                                    edPenulisBuku.setText("")
                                    edTahunBuku.setText("")
                                    edPenerbitBuku.setText("")
                                    edNameImg.setText("")
                                    edNamePdf.setText("")
                                }

                        }
                }

               else if(editPdf){
//                        FirebaseStorage.getInstance().reference.child(fileName1+fileType1).delete()
                        FirebaseStorage.getInstance().reference.child(fileName2+fileType2).delete()
                            fileName2 = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                            val fileRef2 = storage.child(fileName2+fileType2)
                            fileRef2.putFile(uri2)
                                .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task2 ->
                                    return@Continuation fileRef2.downloadUrl
                                })
                                .addOnCompleteListener { taks ->
                                    val hm = HashMap<String, Any>()
                                    hm.set(F_ID, edIdBuku.text.toString())
                                    hm.set(F_JUDUL, edJudulBuku.text.toString())
                                    hm.set(F_PENULIS, edPenulisBuku.text.toString())
                                    hm.set(F_PENERBIT, edPenerbitBuku.text.toString())
                                    hm.set(F_TAHUN, edTahunBuku.text.toString())
                                    hm.put(F_NAME2, fileName2)
                                    hm.put(F_TYPE2,fileType2)
                                    hm.put(F_URL2, taks.result.toString())
                                    db.collection(COLLECTION).document(docId).update(hm)
                                        .addOnSuccessListener {
                                            Toast.makeText(this, "Data Berhasil Updated", Toast.LENGTH_SHORT)
                                                .show()
                                            edIdBuku.setText("")
                                            edJudulBuku.setText("")
                                            edPenulisBuku.setText("")
                                            edTahunBuku.setText("")
                                            edPenerbitBuku.setText("")
                                            edNameImg.setText("")
                                            edNamePdf.setText("")
                                        }.addOnFailureListener { e ->
                                            Toast.makeText(
                                                this,
                                                "Data Tidak Berhasil Updated : ${e.message}",
                                                Toast.LENGTH_SHORT
                                            )
                                                .show()
                                            edIdBuku.setText("")
                                            edJudulBuku.setText("")
                                            edPenulisBuku.setText("")
                                            edTahunBuku.setText("")
                                            edPenerbitBuku.setText("")
                                            edNameImg.setText("")
                                            edNamePdf.setText("")
                                        }
                                }

                }else{
                        val hm = HashMap<String, Any>()
                        hm.set(F_ID, edIdBuku.text.toString())
                        hm.set(F_JUDUL, edJudulBuku.text.toString())
                        hm.set(F_PENULIS, edPenulisBuku.text.toString())
                        hm.set(F_PENERBIT, edPenerbitBuku.text.toString())
                        hm.set(F_TAHUN, edTahunBuku.text.toString())
                        db.collection(COLLECTION).document(docId).update(hm)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data Berhasil Updated", Toast.LENGTH_SHORT)
                                    .show()
                                edIdBuku.setText("")
                                edJudulBuku.setText("")
                                edPenulisBuku.setText("")
                                edTahunBuku.setText("")
                                edPenerbitBuku.setText("")
                                edNameImg.setText("")
                                edNamePdf.setText("")
                            }.addOnFailureListener { e ->
                                Toast.makeText(
                                    this,
                                    "Data Tidak Berhasil Updated : ${e.message}",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                edIdBuku.setText("")
                                edJudulBuku.setText("")
                                edPenulisBuku.setText("")
                                edTahunBuku.setText("")
                                edPenerbitBuku.setText("")
                                edNameImg.setText("")
                                edNamePdf.setText("")
                            }
                    }

            }
            R.id.btHapus -> {
                FirebaseStorage.getInstance().reference.child(fileName1+fileType1).delete()
                FirebaseStorage.getInstance().reference.child(fileName2+fileType2).delete()

                db.collection(COLLECTION).whereEqualTo(F_ID, docId).get()
                    .addOnSuccessListener { results ->
                        for (doc in results) {
                            db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(
                                        this, "Data Berhasil Deleted",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    edIdBuku.setText("")
                                    edJudulBuku.setText("")
                                    edPenulisBuku.setText("")
                                    edTahunBuku.setText("")
                                    edPenerbitBuku.setText("")
                                    edNameImg.setText("")
                                    edNamePdf.setText("")
                                }.addOnFailureListener { e ->
                                    Toast.makeText(
                                        this, "Data Tidak Berhasil Deleted ${e.message}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    edIdBuku.setText("")
                                    edJudulBuku.setText("")
                                    edPenulisBuku.setText("")
                                    edTahunBuku.setText("")
                                    edPenerbitBuku.setText("")
                                    edNameImg.setText("")
                                    edNamePdf.setText("")
                                }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(
                            this, "Can't Get Data References ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.crud)
        alCoba = ArrayList()
        uri1 = Uri.EMPTY
        uri2 = Uri.EMPTY
        btTambah.setOnClickListener(this)
        btUbah.setOnClickListener(this)
        btHapus.setOnClickListener(this)
        btUpImg.setOnClickListener(this)
        btUpPdf.setOnClickListener(this)
        lsCrud.setOnItemClickListener(itemClick)

    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("firestore", e.message.toString())
            ShowData()
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alCoba.get(position)
        docId = hm.get(F_ID).toString()
        edIdBuku.setText(docId)
        edJudulBuku.setText(hm.get(F_JUDUL).toString())
        edPenulisBuku.setText(hm.get(F_PENULIS).toString())
        edPenerbitBuku.setText(hm.get(F_PENERBIT).toString())
        edTahunBuku.setText(hm.get(F_TAHUN).toString())
        fileType1 = hm.get(F_TYPE1).toString()
        fileName1 = hm.get(F_NAME1).toString()
        fileType2 = hm.get(F_TYPE2).toString()
        fileName2 = hm.get(F_NAME2).toString()
        edNameImg.setText(hm.get(F_NAME1).toString()+hm.get(F_TYPE1).toString())
        edNamePdf.setText(hm.get(F_NAME2).toString()+hm.get(F_TYPE2).toString())
    }

    fun ShowData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alCoba.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_JUDUL,doc.get(F_JUDUL).toString())
                hm.set(F_PENULIS,doc.get(F_PENULIS).toString())
                hm.set(F_PENERBIT,doc.get(F_PENERBIT).toString())
                hm.set(F_TAHUN,doc.get(F_TAHUN).toString())
                hm.put(F_NAME1, doc.get(F_NAME1).toString())
                hm.put(F_TYPE1, doc.get(F_TYPE1).toString())
                hm.put(F_URL1, doc.get(F_URL1).toString())
                hm.put(F_NAME2, doc.get(F_NAME2).toString())
                hm.put(F_TYPE2, doc.get(F_TYPE2).toString())
                hm.put(F_URL2, doc.get(F_URL2).toString())
                alCoba.add(hm)
            }
            adapter = CustomAdapter(this,alCoba)
            lsCrud.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK1)){
            if(data != null) {
                uri1 = data.data!!
//                Toast.makeText(this,uri1.toString(),Toast.LENGTH_LONG).show()
                edNameImg.setText(uri1.toString())
                editImg = true
            }
        }
        else if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK2)){
            if(data != null) {
                uri2 = data.data!!
                edNamePdf.setText(uri2.toString())
                editPdf = true
            }
        }
    }

}